"""
Destroy database used for demo (or you can just shut down the Redis serve)
"""
import redis_demo


if __name__ == '__main__':

    config = redis_demo.get_config()
    r = redis_demo.get_connector(config)

    # FLUSHDB
    r.flushdb(config['database'])
