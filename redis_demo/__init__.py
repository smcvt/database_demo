"""
Initialization of Redis connector
"""

import redis
import yaml


CONFIG_FILE = 'config.yaml'
DB = 0


def get_config():
    """
    Get config from YAML file.
    May raise exception if config file missing or ill-formed.
    """
    with open(CONFIG_FILE) as file:
        return yaml.safe_load(file)


def get_connector(c):
    """
    May raise redis.exceptions.ConnectionError if Redis server not running
    or config is invalid. May raise KeyError if bad config.
    """
    return redis.Redis(host=c['host'],
                       port=c['port'],
                       encoding=c['encoding'],
                       decode_responses=c['decode_responses'],
                       db=c['database'])
