"""
Demonstrate basic functionality of Redis
"""
import json
from datetime import datetime

import redis_demo


if __name__ == '__main__':

    config = redis_demo.get_config()
    r = redis_demo.get_connector(config)

    # KEYS *
    v = r.keys("*")
    print(v)  # [] empty list

    # SET foo bar
    r.set("foo", "bar")
    # GET foo
    v = r.get("foo")
    print(v)

    # SET counter 0
    r.set("counter", 0)
    # INCR counter
    r.incr("counter")
    # GET counter
    v = r.get("counter")
    print(v)

    # EXPIRE foo 10
    r.expire("foo", 10)
    v = r.ttl("foo")
    print(v)

    record = {'dob': '2000/01/01',
              'favorite_color': 'chartreuse'}
    # SET charlene with JSON string as value
    r.set('charlene', json.dumps(record))
    # GET charlene
    v = r.get('charlene')
    # load JSON -> dict
    v = json.loads(v)
    print(v)

    # SADD vegetables eggplant, etc.
    r.sadd('vegetables', 'eggplant')
    r.sadd('vegetables', 'basil')
    r.sadd('vegetables', 'brussels sprouts')

    # SADD delicious eggplant, etc.
    r.sadd('delicious', 'eggplant')
    r.sadd('delicious', 'basil')
    r.sadd('delicious', 'ice cream')
    r.sadd('delicious', 'chocolate')

    # get the intersection
    v = r.sinter('vegetables', 'delicious')
    print(v)

    # Round-trip for a datetime object
    dt_format = "%Y-%m-%d %H:%M:%S"
    dt = datetime.now()
    s = dt.strftime(dt_format)
    r.set('time', s)
    s = r.get('time')
    dt = datetime.strptime(s, dt_format)
    print(dt)
