# Database Demo

Demonstrates connection and some simple operations for

* MySQL / MariaDB
* Redis
* CouchDB

May add at a later time:

* SQLite
* RIAK KV
* MongoDB
* PostgreSQL
* ArangoDB
* BerkeleyDB
* ImmuDB
* Rel
* Cassandra

## Python
Assumes a sane, modern Python, _e.g._, Python 3.9 or thereabouts (at least >= 3.6).

I recommend creating a new virtual environment and then running

    pip install -r requirements.txt

## Configuration
Each demo has its own config.
Example config files are supplied for each as `example.yaml`.
These should be copied to `config.yaml` and adjusted to suit your local environment.

## MySQL / MariaDB

Using MySQL Python connector.
See:

* https://pypi.org/project/mysql-connector-python/
* https://dev.mysql.com/doc/connector-python/en/

Assumes MySQL or MariaDB is running locally.

Documentation for MySQL and MariaDB:

* https://dev.mysql.com/doc/
* https://mariadb.org/documentation/

## Redis

Using `redis-py` Redis-Python connector.
See:

* https://pypi.org/project/redis/
* https://github.com/redis/redis-py

Assumes `redis-server` is running locally.

There's also a nice little tutorial at

* https://try.redis.io/

## CouchDB

Using CouchDB2 Python library. 
See: 

* https://pypi.org/project/CouchDB2/
* https://couchdb-python.readthedocs.io/

Assumes CouchDB server is running locally.

With typical configuration, the web interface (Futon) will be available at 

    http://127.0.0.1:5984/_utils

If necessary, add an entry in the `[admins]` section of your `local.ini`.
The location of this file will vary depending on your OS and how you installed CouchDB.
Mine, for example, is at

    /usr/local/etc/local.ini

For CouchDB documenation, see: 

* https://docs.couchdb.org/en/stable/

When tinkering with CouchDB, `curl` can be your friend.
If you don't have `curl` installed, you should consider adding it to your toolkit.
With `curl` you can make requests via the command line, _e.g._,

    curl http://egbert:foobarbaz@127.0.0.1:5984/demo
