"""
Initialization of CouchDB client
"""
import urllib

import yaml
from couchdb2 import Server


CONFIG_FILE = 'config.yaml'


def get_config():
    """
    Get config from YAML file.
    May raise YAML error if file not found or ill-formed.
    """
    with open(CONFIG_FILE) as file:
        return yaml.safe_load(file)


def get_url(c):
    """
    Construct URL from config.
    """
    netloc = f"{c['host']}:{c['port']}"
    return urllib.parse.urlunparse((c['scheme'],
                                    netloc, "/", None, None, None))


def get_server(c):
    """
    May raise CouchDB2Exception if server not running or
    bad config.
    """
    url = get_url(c)
    return Server(href=url,
                  username=c['user'],
                  password=c['password'],
                  use_session=c['use_session'])
