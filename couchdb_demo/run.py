"""Demonstrate CouchDB connection and basic operations"""
import sys

from couchdb2 import CouchDB2Exception, NotFoundError

import couchdb_demo


if __name__ == '__main__':

    config = couchdb_demo.get_config()
    server = couchdb_demo.get_server(config)

    try:
        db = server.get("demo", check=True)
    except NotFoundError:
        db = server.create('demo')

    try:
        doc = {'_id': '1234',
               'title': 'Basho\'s Frog',
               'author': 'Matsuo Basho',
               'author_years': '1643--1694',
               'translator': 'Allan Ginsberg',
               'body': 'The old pond\n'
                       'A frog jumped in\n'
                       'Kerplunk!',
               'source_url': 'http://www.bopsecrets.org/gateway/passages/basho-frog.htm'}
        db.put(doc)
        # Now the document is in the database

        # Now fetch the document
        doc = db['1234']
        print(doc)

        # Now another. Notice this has keys not in the previous doc.
        doc = {'_id': '3456',
               'title': 'The Light of a Candle',
               'author': 'Yosa Buson',
               'author_years': '1716--1784',
               'translator': 'Edith Schiffert',
               'body': 'The light of a candle\n'
                        'Is transferred to another candle—\n'
                        'spring twilight.',
               'copyright': 2007,
               'permission': 'White Pine Press',
               'souce_url': 'https://poets.org/poem/light-candle'}
        db.put(doc)

        # Design doc
        # From the CouchDB docs: "In CouchDB, design documents provide the
        # main interface for building a CouchDB application. The design
        # document defines the views used to extract information from
        # CouchDB through one or more views. Design documents are created
        # within your CouchDB instance in the same way as you create database
        # documents, but the content and definition of the documents is
        # different. Design Documents are named using an ID defined with
        # the design document URL path, and this URL can then be used to
        # access the database contents.
        js = 'function (doc) { ' \
             '  emit(doc.title, null); ' \
             '}'
        js2 = 'function (doc) { ' \
             '  emit(doc.title, doc.author); ' \
             '}'
        db.put_design('simple',
                      {'views':
                           {'by_title': {'map': js},
                            'all': {'map': js2}
                            }
                       })

        # Use the design doc to fetch by title
        result = db.view('simple',
                         'by_title',
                         key='Basho\'s Frog',
                         include_docs=True)
        print(result[0].doc)

        # Use the design doc to fetch all docs
        result = db.view('simple', 'all')
        for doc in result:
            print(doc)
        # Try visiting http://127.0.0.1:5984/demo/_design/simple/_view/all

        # A query to find all docs with author = Matsuo Basho
        selector = {'author': 'Matsuo Basho'}
        result = db.find(selector)
        print(result)

    except CouchDB2Exception as e:
        print(e)
        sys.exit(1)
