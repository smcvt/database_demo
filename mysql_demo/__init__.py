"""
Initialization of MySQL client
"""
from mysql import connector
import yaml

CONFIG_FILE = 'config.yaml'


def get_config():
    """
    Get config from YAML file.
    May raise YAML error if file not found or ill-formed.
    """
    with open(CONFIG_FILE) as file:
        return yaml.safe_load(file)


def get_connector(c):
    """
    May raise DatabaseError if server not running or bad config,
    or KeyError if bad config.
    """
    return connector.connect(host=c['host'],
                             user=c['user'],
                             password=c['password'])
