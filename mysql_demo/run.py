"""
Demonstration of connection and some simple operations with MySQL
"""

import mysql_demo


if __name__ == '__main__':

    config = mysql_demo.get_config()
    cnx = mysql_demo.get_connector(config)

    cursor = cnx.cursor()
    cursor.execute("CREATE SCHEMA IF NOT EXISTS demo;")
    cursor.execute("USE demo;")
    cursor.execute("CREATE TABLE vegetable ("
                   "PRIMARY KEY (genus, species),"
                   "common_name VARCHAR(31),"
                   "genus VARCHAR(31),"
                   "species VARCHAR(31));")
    cursor.execute("INSERT INTO vegetable"
                   "(common_name, genus, species)"
                   "VALUES ('eggplant', 'Solanum', 'melongena'),"
                   "('basil', 'Omicum', 'basilicum');")
    cursor.execute("SELECT * FROM vegetable;")

    row = cursor.fetchone()
    while row is not None:
        print(row)
        row = cursor.fetchone()

    cursor.close()
    cnx.close()
