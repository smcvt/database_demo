import mysql_demo

if __name__ == '__main__':

    config = mysql_demo.get_config()
    cnx = mysql_demo.get_connector(config)

    cursor = cnx.cursor()

    cursor.execute("DROP SCHEMA IF EXISTS demo;")
